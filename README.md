# User Numbers

Application for setting a favorite number for a given user.

## What is this all about

The application has a single exposed web socket that serves two types of messages. One for fetching list of all users and their favorite numbers, 
ordered alphabetically and the other for setting a favorite number for particular user.

Upon update of any users favorite number, the updated list is delivered to all established socket connections.


### Architecture

The project has 3 layers.

1. Web layer, accepting and handling websocket connections
2. Broker and data store layer, implemented by REDIS (official REDIS docker image)
3. Worker layer, handling the request messages, initiate data persistence

## Built With

* [Go](https://golang.org/) - Golang programming language
* [REDIS](https://redis.io/) - REDIS data structure store
* [Gorilla](https://github.com/gorilla/websocket/) - Gorilla WebSocket protocol
* [Docker](https://www.docker.com/) - Docker

### Prerequisites

It requires installed docker and docker compose.

For Linux guys, do this:
```
$ sudo apt-get update
$ sudo apt-get install docker-ce=<VERSION>
$ sudo apt-get install docker-compose=<VERSION>
```

For Mac users go here:
```
https://docs.docker.com/docker-for-mac/install/
https://docs.docker.com/compose/install/
```

For Windows guys go here:
```
https://docs.docker.com/docker-for-windows/install/
https://docs.docker.com/compose/install/
```

### Installation

Check out the project

```
git clone https://gitlab.com/markolini/user-numbers.git
```

and initiate docker compose

```
docker-compose run
```

## Getting Started

The index page is located at URL:

```
localhost:8090
```

Enjoy. :)
