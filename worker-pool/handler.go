package main

import (
	"log"
	"encoding/json"
	"github.com/go-redis/redis"
)

func tasksEventListener(message *redis.Message) {
	log.Printf("Received message %v", message)

	payload := message.Payload

	log.Printf("Payload is %v", payload)

	persistToRedis(payload)
	broadcastUpdateEvent()
}

func persistToRedis(payload string) {
	log.Printf("Persisting to redis %v", payload)

	user := &User{}
	err := json.Unmarshal([]byte(payload), user)
	if err != nil {
		log.Printf("Error unmarshalling a USER %v", err)
		return
	}

	client.HSet("users", user.Username, payload)
}

func broadcastUpdateEvent() {
	log.Println("Pushing event")
	client.Publish("events", "UPDATED")
}
