package main

import (
	"log"
	"flag"
	"net/http"
	"github.com/go-redis/redis"
	"os"
)

var (
	client *redis.Client
	addr   = flag.String("addr", ":8080", "http service address")
)

func main() {
	client = redis.NewClient(&redis.Options{
		Addr: os.Getenv("REDIS_URL"),
	})

	tasksSub := client.Subscribe("tasks")
	tasksChannel := tasksSub.Channel()

	for {
		go tasksEventListener(<-tasksChannel)
	}

	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal(err)
	}
}
