package main

type User struct {
	Username string `json:"username"`
	Number   int    `json:"number"`
}

type ByUsername []User

func (v ByUsername) Len() int           { return len(v) }
func (v ByUsername) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v ByUsername) Less(i, j int) bool { return v[i].Username < v[j].Username }
