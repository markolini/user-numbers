package main

import (
	"log"
	"sync"
	"net/http"
	"github.com/gorilla/websocket"
)

type Connection struct {
	ws *websocket.Conn
}

type ConnectionManager struct {
	mutex        sync.RWMutex
	eventChannel chan *[]User
	connections  map[*websocket.Conn]*Connection
}

func createManager() *ConnectionManager {
	return &ConnectionManager{
		connections:  make(map[*websocket.Conn]*Connection),
		eventChannel: make(chan *[]User),
	}
}

func (manager *ConnectionManager) ServeHTTP(rsp http.ResponseWriter, req *http.Request) {
	socket, socketErr := wsUpgrade.Upgrade(rsp, req, nil)
	if socketErr != nil {
		log.Print("Error during socket upgrade:", socketErr)
		return
	}

	manager.Accept(socket)
}

func (manager *ConnectionManager) Accept(connection *websocket.Conn) {
	defer manager.cleanup(connection)

	conn := &Connection{ws: connection}

	manager.mutex.Lock()
	manager.connections[connection] = conn
	manager.mutex.Unlock()

	go broadcastOnUpdate(manager)
	handleMessage(manager, connection)
}

func (manager *ConnectionManager) cleanup(connection *websocket.Conn) {
	connection.Close()
	manager.mutex.Lock()

	delete(manager.connections, connection)

	manager.mutex.Unlock()
}

func (manager *ConnectionManager) broadcast(response Response) {
	for connection := range manager.connections {
		err := connection.WriteJSON(response)
		if err != nil {
			log.Printf("Error during brodcast: %v", err)
			connection.Close()
			delete(manager.connections, connection)
		}
	}
}
