package main

import (
	"log"
	"fmt"
	"sort"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"github.com/gorilla/websocket"
)

type Request struct {
	Payload User   `json:"payload"`
	Type    string `json:"type"`
}

type Response struct {
	Payload []User `json:"payload"`
}

func createResponse() *Response {
	return &Response{Payload: []User{}}
}

func indexHandler(w http.ResponseWriter, _ *http.Request) {
	content, err := ioutil.ReadFile("index.html")
	if err != nil {
		log.Println("Could not open file.", err)
	}

	fmt.Fprintf(w, "%s", content)
}

func handleMessage(manager *ConnectionManager, connection *websocket.Conn) {
	for {
		request := &Request{}
		jsonReadErr := connection.ReadJSON(request)
		if jsonReadErr != nil {
			log.Printf("Error reading message from socket %v", jsonReadErr)
			delete(manager.connections, connection)
			return
		}

		switch request.Type {
		case "GET":
			users, err := fetchUsers()
			if err != nil {
				return
			}
			respond(connection, users)
		case "SET":
			setUsersFavoriteNumber(request.Payload)
		default:
			log.Println("Unknown request TYPE")
			return
		}
	}
}

func respond(connection *websocket.Conn, users []User) {
	err := connection.WriteJSON(users)
	if err != nil {
		log.Printf("Error fetching USERS: %v", err)
		connection.Close()
	}
}

func setUsersFavoriteNumber(payload User) {
	userJson, _ := json.Marshal(payload)
	log.Printf("Got message: %#v\n", userJson)
	client.Publish("tasks", userJson)
}

func fetchUsers() ([]User, error) {
	jsonUsers, fetchErr := client.HGetAll("users").Result()
	if fetchErr != nil {
		log.Printf("Error fetching USERS %v", fetchErr)
		return nil, fetchErr
	}

	mappedUsers := make([]User, 0)
	user := &User{}

	for _, usr := range jsonUsers {
		err := json.Unmarshal([]byte(usr), user)
		if err != nil {
			log.Printf("Error unmarshalling a USER %v", err)
			return nil, err
		}

		mappedUsers = append(mappedUsers, *user)
	}

	sort.Sort(ByUsername(mappedUsers))
	return mappedUsers, nil
}

func broadcastOnUpdate(manager *ConnectionManager) {
	for {
		log.Printf("Received update signal %v", <-eventsChannel)

		mappedUsers, fetchErr := fetchUsers()
		if fetchErr != nil {
			return
		}

		response := createResponse()
		response.Payload = append(response.Payload, mappedUsers...)
		manager.broadcast(*response)
	}
}
