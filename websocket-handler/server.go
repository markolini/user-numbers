package main

import (
	"log"
	"flag"
	"net/http"
	"github.com/go-redis/redis"
	"github.com/gorilla/websocket"
	"os"
)

var (
	client        *redis.Client
	eventsChannel <-chan *redis.Message
	addr          = flag.String("addr", ":8090", "http service address")
	wsUpgrade     = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(request *http.Request) bool {
			return true
		},
	}
)

func main() {
	client = redis.NewClient(&redis.Options{
		Addr: os.Getenv("REDIS_URL"),
	})

	eventsSub := client.Subscribe("events")
	eventsChannel = eventsSub.Channel()

	wsHandler := createManager()

	http.HandleFunc("/", indexHandler)
	http.Handle("/ws", wsHandler)

	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal(err)
	}
}
